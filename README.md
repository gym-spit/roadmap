# E-Špit

# O co se snažíme
## KOS-like projekt
- přihlašování předmětů
- přihlašování na jednorázové akce
- přihlašování obědů? - v ankětě [mapa školy](https://gymspit.cz/uploads/Mapa_skoly_4ec9ecc171.pdf), str. 5, 12 a 14 si na to dost lidí stěžuje
    - feature do šuplíku, můžeme ho napsat a nechat vypnuté, dokud nenanejde správný čas
- ankety - studentská rada?
- wiki?
	- loňská nabídka volitelných předmětů a jejich sylabus - (anonymní?) reference projdivších studentů?
	- návody
        - přihlášení na wifi - Android/iOS, Win/Mac/Chrome/Linux
	    - zapomenuté heslo do bakalářů
	    - návod na Office
	- seznam učebnic
- API - možnost zaregistrovat jakoukoli aplikaci pro ověřování před gymšpit oauth proxy
    - nemuselo by být tak těžké implementovat, stejně musíme s DB studentů pracovat, takto budeme pouze info vystavovat registrovaným aplikacím přes JWT endpoint
- discord?
- generování dokumentů - potvrzení o studiu, přihláška na lyžák a podobně - to ale pracuje s rodným číslem, adresou a podobně - máme právo s těmihle věcmi nakládat? není to už moc?
- generování/pomoc s tvorbou rozvrhu - momentálně je to prý nehorázný pain, byla by to výzva, ale určitě by to vedení ocenilo

## pohledy
- student - stejně jako v kosu, vidí jen ty přihlašovací akce, které jsou zrovna aktuální
- učitel - může vytvořit novou akci, třídní vidí zapsané předměty své třídy
- vedení - učitel++
- admin - cokoli se rozesere aby bylo vidět, turn on/off features, logy a jejich filtrování
- API kdokoliv - možnost zakázat, pokud tam bude dávat hovadiny

# technikálie
- hostování na [GitLabu](https://gitlab.com/gym-spit)
- CSS řešíme přes bootstrap a SASS?
- FE i BE v ASP.NET **Core** - modularita, na PG se učí C#
- je potřeba perfektní dokumentace a **ultra mega hyper super perfektní uživatelský manuál** - projít si [user guide tools](https://www.google.com/search?q=best+user+guide+software), popř. MD a Writerside
- logy, logy, logy

# Jak to konkrétně vypadá
## pohledy
### přihlašování předmětů
#### student
- zaškrtávací políčka volitelných předmětů, loňský/stálý vyučující, ideálně proklik na wiki se sylabem předmětu
	- tabulka - lidi vidí, na čem jsou ostatní - sociální prvek, můžou být s kamarády, zase se ale někdo nemusí mít možnost rozhodnout podle sebe a cítit se, že někam musí, když jsou tam všichni
	- zaškrtávací políčka - každý je v tom sám a rozhoduje se na základě vlastní hlavy, zase když ale chce někde být s někým, tak se musí zeptat, to je předmět k diskuzi (a taky jsou na bootstrapu)
	- kos-like vzhled, viz obrázek
	- proč sylabus - např. fyzika ve 4 je nalévárna k maturitě, což někdo nevěděl, Standova fyzika je zas nejvíc chill předmět, ale lidi tam nechtěli, chem s mal jsou sice dost laborky, ale dost teorie
- má 3 povinné, možnost uložení, že se s dalšími rozhodne později
	- možnost ok, uzavřít zápis, nedovolí, pokud nejsou zaškrtnuté 3 předměty
	- asi lze potom měnit, ale aby tam byl ten rozhodující moment "jsi s tím v pohodě?" a lidi si to neuložili a pak na to nezapomněli...
	- možnost přidat i další (+2?), jinou barvou, vedení ví, že jsou navíc a nepovinné

#### třídní učitel
- skutečně tabulka, vidí, kdo kde je, kdo už má 3, kdo ne, kdo má víc - možnost se žáka zeptat, jestli to fakt zvládne, možná vidí i uložené volby?
- filtrování - tenhle to ještě neotevřel, tenhle ještě nemá vyplněné všechno, tenhle to neuzavřel, tihle to už mají
- v moment, co mají vyplněno a odevzdáno všichni (oznámení), odesílá učitel tabulku vedení

#### vedení
- vytváří nabídku předmětů, které se otevřou - možnost vzít si loňskou (musíme napoprvé naklikat)
- v moment přijetí od třídního (oznámení) vidí celou tabulku a může přepisovat
- zvýraznění právě zapisovaného řádku žáka, sloupce předmětu, možnost řadit/zobrazit jen ty žáky, co mají určitý předmět, **export do pdf pro tisk** - pomocí [pfdsharp](http://pdfsharp.net/), statistiky (20 lidí na PG, musíme otevřít 2 paralelky), asi možnost zvýraznění políčka na vyřešení později - TODO-like, zobrazuje se někde nahoře v oznámeních? to bude pain, ale třeba by se to líbilo

### přihlašování na akce
#### učitel/vedení
- vytvoří akci, může přidat spoluodpovědnou osobu - typicky Boušková + Blažková pro PD apod, obě mají rovná práva
- vybere, koho se akce týká - DP typicky ne 1-3 ročníku
- vybere šablonu - DP, PD - jednotlivé dílny, kapacita dílen, prostor na anotaci dílny
- zveřejnění akce buď okamžitě, nebo v naplánovaný čas
- možnost poslat oznámení žákům, automaticky se pošle v moment otevření
- zobrazení přihlášených žáků po třídách, po jednotlivých dílnách apod, potřeba konzultovat
- možná zobrazit až po určitém čase, když bude Boušková sledovat, jak se všichni najednou snaží přihlásit, bude se jí to pořád aktualizovat a ubírat traffic

#### student
- přijde mu oznámení, že byla otevřena akce
- pohled jako v kosu viz obrázek, tlačítka jednotlivých bloků
- v moment přihlášení dílny sync na server, aktualizace volných míst

## vlastní knihovna pro práci s databází
- core feature - vyplatí se zpracovat jako knihovnu
- **SQLite**? MariaDB? PostgreSQL?
- DB logů
- DB uživatelů
	- studentů a odpovídajících tříd - buď při prvním přihlášení, nebo v rámci **každoročního syncu**
        - jak to má Azure? Podle všeho by se muselo parsovat dle přislušnosti ke skupině s určitým ID, nebylo by jednodušší přistoupit do bakalářů? Nebo vytáhnout CSV nových tříd
	- učitelů a jejich tříd
	- adminů
- DB akcí
- DB aplikací povolených k přístupu k API
- DB článků na wiki? to by možná chtělo nějaký CMS, nebo hugo rendering - opět modularita
- DB DC uživatelů?

> Přihlašování na semináře se otevírá v druhém pololetí - buď to udělat do konce února, nebo na to pak máme celý rok až do DP. Nepřijde mi to nereálné, ale kdo ví.
